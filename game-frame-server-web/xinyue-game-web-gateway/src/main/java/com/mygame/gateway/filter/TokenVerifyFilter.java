package com.mygame.gateway.filter;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import com.mygame.common.GameConstants;
import com.mygame.common.error.TokenException;
import com.mygame.common.model.AccountToken;
import com.mygame.common.utils.JWTUtil;
import reactor.core.publisher.Mono;

/**
 * 
 * @ClassName: TokenVerifyFilter
 * @Description: token验证的filter，用户登陆成功之后，以后再访问服务需要对token进行验证。
 * @author: wgs
 * @date: 2019年3月15日 下午8:01:58
 */
@Service
public class TokenVerifyFilter implements GlobalFilter, Ordered {

    @Autowired
    private FilterConfig filterConfig;

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE - 1;
    }

    private Logger logger = LoggerFactory.getLogger(TokenVerifyFilter.class);

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String requestUri = exchange.getRequest().getURI().getPath();
        List<String> whiteRequestUris = filterConfig.getWrhiteRequestUri();
        if (whiteRequestUris.contains(requestUri)) {
            return chain.filter(exchange);// 如果请求的uri在白名单中，则跳过验证。
        }

        String token = exchange.getRequest().getHeaders().getFirst(GameConstants.USER_TOKEN);
        if (StringUtils.isEmpty(token)) {
            logger.debug("{} 请求验证失败,token为空", requestUri);
            exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
            return exchange.getResponse().setComplete();
        }

        try {
          AccountToken accountToken =  JWTUtil.getTokenContent(token,AccountToken.class);
          logger.debug("{} token 验证成功",accountToken.getAccountId());
            return chain.filter(exchange);
        } catch (TokenException e) {
            logger.debug("{} 请求验证失败,token非法",token,e);
            exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
            return exchange.getResponse().setComplete();
        }

    }

}
