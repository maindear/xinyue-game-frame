package com.mygame.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameWebServerGatewayMain {
    public static void main(String[] args) {
        SpringApplication.run(GameWebServerGatewayMain.class, args);
    }
}
