package com.xinyue.center.messages.request;

import org.springframework.util.StringUtils;

import com.xinyue.center.messages.GameCenterError;
import com.xinyue.network.message.web.AbstractHttpRequestParam;

public class CreatePlayerParam extends AbstractHttpRequestParam {

    private String nickName;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }


    @Override
    protected void haveError() {
         if (StringUtils.isEmpty(nickName)) {
            this.error = GameCenterError.NICKNAME_IS_EMPTY;
        } else {
            int len = nickName.length();
            if (len < 2 || len > 10) {
                this.error = GameCenterError.NICKNAME_LEN_ERROR;
            }
        }
    }



}
