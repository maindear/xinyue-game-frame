package com.xinyue.center.service;

import java.util.Optional;
import com.xinyue.center.dao.entity.Account;
import com.xinyue.center.messages.request.LoginParam;

public interface IAccountService {

	Optional<Account> login(LoginParam loginParam);

	Account register(LoginParam loginParam);

	String createUserToken(Account account);

	Account selectAccount(String accountId);

	void updateAccount(Account account);
	
	

}
