package com.xinyue.center;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;


@SpringBootApplication
@EnableMongoRepositories("com.xinyue.center.dao")
public class WebGameCenterServerMain {
    
    public static void main(String[] args) {
		SpringApplication.run(WebGameCenterServerMain.class, args);
    }
}
