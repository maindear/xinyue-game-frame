package com.xinyue.center.messages.request;

import org.springframework.util.StringUtils;
import com.xinyue.center.messages.GameCenterError;
import com.xinyue.network.message.web.AbstractHttpRequestParam;

public class LoginParam extends AbstractHttpRequestParam {
    private int loginType;// 登陆类型：1 用户名密码；2 第三方sdk
    private String userName;
    private String password;
    private String loginIp;


    public boolean isUserNameLogin() {
        return this.loginType == 1;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public int getLoginType() {
        return loginType;
    }

    public void setLoginType(int loginType) {
        this.loginType = loginType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    @Override
    protected void haveError() {

        if (StringUtils.isEmpty(userName)) {
            this.error = GameCenterError.USERNAME_NULL;
        } else if (userName.length() > 32) {
            this.error = GameCenterError.USERNAME_LENGTH_ERROR;
        } else if (StringUtils.isEmpty(password)) {
            this.error = GameCenterError.PASSWORD_NULL;
        } else if (password.length() > 32) {
            this.error = GameCenterError.PASSWORD_LENGTH_ERROR;
        }
        if (this.loginType != 1 && this.loginType != 2) {
            this.error = GameCenterError.LOGIN_TYPE_ERROR;
        }


    }


}
