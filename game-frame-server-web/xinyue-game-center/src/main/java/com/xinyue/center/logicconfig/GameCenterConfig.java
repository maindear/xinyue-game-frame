package com.xinyue.center.logicconfig;

import org.apache.commons.lang3.RandomUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "gamecenter.config")
public class GameCenterConfig {

	private int localServerId = RandomUtils.nextInt(10001, 30001);
    private int userTokenExpire = 7;
    private int playerTokenExpire = 7;
    /**
     * dao异步操作的线程数，默认是4
     */
    private int daoAsyncThreadCount = 4;
    
    
    public int getDaoAsyncThreadCount() {
        return daoAsyncThreadCount;
    }
    public void setDaoAsyncThreadCount(int daoAsyncThreadCount) {
        this.daoAsyncThreadCount = daoAsyncThreadCount;
    }
    public int getUserTokenExpire() {
        return userTokenExpire;
    }
    public void setUserTokenExpire(int userTokenExpire) {
        this.userTokenExpire = userTokenExpire;
    }
    public int getPlayerTokenExpire() {
        return playerTokenExpire;
    }
    public void setPlayerTokenExpire(int playerTokenExpire) {
        this.playerTokenExpire = playerTokenExpire;
    }
	public int getLocalServerId() {
		return localServerId;
	}
	public void setLocalServerId(int localServerId) {
		this.localServerId = localServerId;
	}
    
    
}
