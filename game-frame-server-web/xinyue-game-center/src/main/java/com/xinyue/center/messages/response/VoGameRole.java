package com.xinyue.center.messages.response;

public class VoGameRole {
    
    private long playerId;
    private String nickname;
    

    
    public long getPlayerId() {
        return playerId;
    }
    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }
    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    
   
}
