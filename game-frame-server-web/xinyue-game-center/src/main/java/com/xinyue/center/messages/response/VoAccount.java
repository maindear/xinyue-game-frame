package com.xinyue.center.messages.response;

import java.util.ArrayList;
import java.util.List;

public class VoAccount {

    private String username;
    private String  accountId;
    private long createTime;
    // 记录已创建角色的基本信息
    private List<VoGameRole> gameRoles = new ArrayList<>();
    private String token;
    private long playerId;
    
    
	public long getPlayerId() {
		return playerId;
	}
	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}
	public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
   
    public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public long getCreateTime() {
        return createTime;
    }
    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
	public List<VoGameRole> getGameRoles() {
		return gameRoles;
	}
	public void setGameRoles(List<VoGameRole> gameRoles) {
		this.gameRoles = gameRoles;
	}
  
 
    
    
}
