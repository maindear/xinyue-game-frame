package com.xinyue.center.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.alibaba.nacos.common.utils.Md5Utils;
import com.xinyue.center.dao.entity.Account;
import com.xinyue.center.dao.redis.EnumRedisKey;
import com.xinyue.framework.dao.AbstractDao;
import com.xinyue.framework.dao.redis.IRedisKeyConfig;

@Repository
public class AccountDao extends AbstractDao<Account, String> {
	@Autowired
	private AccountRepository accountRepository;

	@Override
	protected IRedisKeyConfig getRedisKey() {
		return EnumRedisKey.Account;
	}

	@Override
	protected MongoRepository<Account, String> getMongoRepository() {
		return accountRepository;
	}

	@Override
	protected Class<Account> getEntityClass() {
		return Account.class;
	}

	/**
	 * 
	 * <p>
	 * Description:使用算法，根据用户名生成一个唯的ID
	 * </p>
	 * 
	 * @param username
	 * @return
	 * @author wang guang shuai
	 * @date 2020年3月13日 下午3:39:24
	 *
	 */
	public String useranmeGenerateAccountId(String username) {
		String value = username + username.toLowerCase();
		String accountId = Md5Utils.getMD5(value, "utf8");
		return accountId;
	}
}
