package com.xinyue.center.dao.entity;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Account")
public class Account {
    @Id
    private String  accountId;
    //生成一个唯一的角色id,在游戏服中，如果不分区，就直接用它做为角色id，如果分区，分区id+playerId做为角色id
    private long playerId;
    private String username;
    private String password;
    //注册时间，单位是秒
    private long createTime;
    private String registerIP;
    
    private List<GameRole> gameRoles = new ArrayList<>();
    
    
    public long getPlayerId() {
		return playerId;
	}
	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}
	public String getAccountId() {
        return accountId;
    }
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public long getCreateTime() {
        return createTime;
    }
    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
    public String getRegisterIP() {
        return registerIP;
    }
    public void setRegisterIP(String registerIP) {
        this.registerIP = registerIP;
    }
    public List<GameRole> getGameRoles() {
        return gameRoles;
    }
    public void setGameRoles(List<GameRole> gameRoles) {
        this.gameRoles = gameRoles;
    }
    
    
    
}
