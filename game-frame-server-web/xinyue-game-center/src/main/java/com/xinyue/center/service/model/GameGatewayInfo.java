package com.xinyue.center.service.model;

import com.google.common.base.Objects;

public class GameGatewayInfo {
    private int id; // 唯一id
    private String ip; // 网关ip地址
    private int port; // 网关端口
    private int httpPort;//网关服务的Http的服务地址

    @Override
    public String toString() {
        return "GameGatewayInfo [id=" + id + ", ip=" + ip + ", port=" + port + ", httpPort=" + httpPort + "]";
    }
    @Override
    public int hashCode() {
    	return Objects.hashCode(id,this.getClass().getName());
    }
    @Override
    public boolean equals(Object obj) {
    	if(obj == null) {
    		return false;
    	}
    	if(obj == this) {
    		return true;
    	}
    	
    	GameGatewayInfo value = (GameGatewayInfo) obj;
    	
    	return this.ip.equals(value.ip) && this.httpPort == value.httpPort && this.port == value.port;
    }

    public int getHttpPort() {
        return httpPort;
    }

    public void setHttpPort(int httpPort) {
        this.httpPort = httpPort;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
