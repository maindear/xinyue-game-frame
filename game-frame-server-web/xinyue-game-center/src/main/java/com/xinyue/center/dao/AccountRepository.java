package com.xinyue.center.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.xinyue.center.dao.entity.Account;

public interface AccountRepository extends MongoRepository<Account, String>{
}
