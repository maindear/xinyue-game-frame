package com.xinyue.center.dao.redis;

import java.time.Duration;
import com.xinyue.framework.dao.redis.IRedisKeyConfig;

public enum EnumRedisKey implements IRedisKeyConfig {
	Account("account", Duration.ofDays(7)), PlayerUUID("playerUUID", null),

	;
	private String key;
	private Duration duration;

	public static String Namespace="center-default-namespace";

	private EnumRedisKey(String key, Duration duration) {
		this.key = key;
		this.duration = duration;
	}

	@Override
	public String getKey(String id) {

		return this.getKey() + "_" + id;
	}

	@Override
	public String getKey() {
		return Namespace + ":" + this.key;
	}

	@Override
	public Duration getExpire() {
		return duration;
	}

}
