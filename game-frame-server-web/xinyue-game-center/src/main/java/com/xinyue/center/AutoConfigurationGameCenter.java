package com.xinyue.center;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.mygame.common.concurrent.GameEventExecutorGroup;
import com.xinyue.center.dao.redis.EnumRedisKey;
import com.xinyue.center.logicconfig.GameCenterConfig;
import com.xinyue.framework.dao.DefaultGameDaoEventExecutorFactory;
import com.xinyue.framework.dao.IGameDaoExecutorFactory;

@Configuration
public class AutoConfigurationGameCenter {

	@Autowired
	private GameCenterConfig gameCenterConfig;
	private GameEventExecutorGroup eventExecutorGroup;

	@Autowired
	private NacosDiscoveryProperties nacosDiscoveryProperties;

	@PostConstruct
	public void init() {
		eventExecutorGroup = new GameEventExecutorGroup(gameCenterConfig.getDaoAsyncThreadCount(), "DB-Executor");
		EnumRedisKey.Namespace = nacosDiscoveryProperties.getNamespace();
	}

	@Bean
	public IGameDaoExecutorFactory gameDaoExecutorFactory() {
		// 提供一个特定的数据库操作线程池
		return new DefaultGameDaoEventExecutorFactory(eventExecutorGroup);
	}
}
