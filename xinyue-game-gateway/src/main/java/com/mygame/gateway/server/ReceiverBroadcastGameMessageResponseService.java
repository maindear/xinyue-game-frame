package com.mygame.gateway.server;

import javax.annotation.PostConstruct;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 * 
 * @ClassName: GameMessageConsume
 * @Description: 接收业务服务广播返回的消息，并发送到客户端
 * @author: wgs
 * @date: 2019年5月15日 上午9:17:48
 */
@Service
//监听服务广播的消息，使用一个固定的topic即可。
@RocketMQMessageListener(topic = "${spring.cloud.nacos.discovery.namespace}-XinyueBusinessTopic", consumerGroup = "${spring.cloud.nacos.discovery.namespace}-XinyueGatewayConsumeBroakcastGroup-${xinyue.game.common.config.local-server-id}")
public class ReceiverBroadcastGameMessageResponseService implements RocketMQListener<MessageExt> {
	private Logger logger = LoggerFactory.getLogger(ReceiverBroadcastGameMessageResponseService.class);
	@Autowired
	private ChannelService channelService;
	@Autowired
	private Environment environment;

	@PostConstruct
	public void init() {
		RocketMQMessageListener rocketMQMessageListener = this.getClass().getAnnotation(RocketMQMessageListener.class);
		if (rocketMQMessageListener == null) {
			logger.error("没有添加消费消息监听");
			return;
		}
		logger.info("监听业务广播消息topic:{},group:{}",
				environment.resolveRequiredPlaceholders(rocketMQMessageListener.topic()),
				environment.resolveRequiredPlaceholders(rocketMQMessageListener.consumerGroup()));
	}

	@Override
	public void onMessage(MessageExt message) {
		ReceiverGameMessageResponseService.sendMessage(message, channelService);
	}
}
