package com.mygame.gateway.server;

import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;

import org.apache.rocketmq.remoting.common.RemotingUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.mygame.gateway.common.GatewayServerConfig;
import com.mygame.gateway.server.handler.ConfirmHandler;
import com.mygame.gateway.server.handler.DispatchGameMessageHandler;
import com.mygame.gateway.server.handler.HeartbeatHandler;
import com.mygame.gateway.server.handler.codec.DecodeHandler;
import com.mygame.gateway.server.handler.codec.EncodeHandler;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

@Service
public class GatewayServerBoot {
	private Logger logger = LoggerFactory.getLogger(GatewayServerBoot.class);

	private NioEventLoopGroup bossGroup = null;
	private NioEventLoopGroup workerGroup = null;

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private GatewayServerConfig serverConfig;// 注入网关服务配置

	public void startServer() {
		bossGroup = new NioEventLoopGroup(serverConfig.getBossThreadCount());
		workerGroup = new NioEventLoopGroup(serverConfig.getWorkThreadCount());
		int port = this.serverConfig.getServerPort();
		ServerBootstrap bootstrap = new ServerBootstrap();
		try {
			bootstrap.group(bossGroup, workerGroup)
					.channel(useEpoll() ? EpollServerSocketChannel.class : NioServerSocketChannel.class)
					.option(ChannelOption.SO_BACKLOG, 1024).option(ChannelOption.SO_REUSEADDR, true)
					.option(ChannelOption.SO_KEEPALIVE, false).childOption(ChannelOption.TCP_NODELAY, true)
					.childOption(ChannelOption.SO_SNDBUF, serverConfig.getSendBufSize())
					.childOption(ChannelOption.SO_RCVBUF, serverConfig.getRecBufSize())
					.childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
					.localAddress(new InetSocketAddress(port)).childHandler(createChannelInitializer());
			logger.info("开始启动服务，端口:{}", port);
			ChannelFuture f = bootstrap.bind(port);
			f.addListener(new GenericFutureListener<Future<? super Void>>() {
				@Override
				public void operationComplete(Future<? super Void> future) throws Exception {
					logger.info("----网关服务器启动成功---");
				}
			});
			f.channel().closeFuture().sync();
		} catch (Exception e) {
			logger.error("服务器启动失败", e);
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}

	private boolean useEpoll() {
		return RemotingUtil.isLinuxPlatform() && Epoll.isAvailable();
	}

	private ChannelInitializer<Channel> createChannelInitializer() {// 连接channel初始化的时候调用
		ChannelInitializer<Channel> channelInitializer = new ChannelInitializer<Channel>() {
			@Override
			protected void initChannel(Channel ch) throws Exception {
				ChannelPipeline p = ch.pipeline();
				try {

					int allIdleTimeSeconds = serverConfig.getServerChannelMaxIdleTimeSeconds();
					p.addLast(new IdleStateHandler(0, 0, allIdleTimeSeconds));
					p.addLast("EncodeHandler", new EncodeHandler());// 添加编码Handler
					p.addLast(new LengthFieldBasedFrameDecoder(1024 * 8, 0, 4, -4, 0));// 添加拆包
					p.addLast("DecodeHandler", new DecodeHandler());// 添加解码
					p.addLast("ConfirmHandler", new ConfirmHandler(applicationContext));
					p.addLast("HeartbeatHandler", new HeartbeatHandler());
					p.addLast(new DispatchGameMessageHandler(applicationContext));
				} catch (Exception e) {
					ch.close();
					logger.error("创建channel失败", e);
				}
			}
		};
		return channelInitializer;
	}

	public void shutdownGatewayServer() {
		int quietPeriod = 5;
		int timeout = 30;
		TimeUnit timeUnit = TimeUnit.SECONDS;
		if (workerGroup != null) {
			workerGroup.shutdownGracefully(quietPeriod, timeout, timeUnit);
		}
		if (bossGroup != null) {
			bossGroup.shutdownGracefully(quietPeriod, timeout, timeUnit);
		}
	}
}
