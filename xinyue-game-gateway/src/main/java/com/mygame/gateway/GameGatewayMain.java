package com.mygame.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import com.mygame.gateway.server.GatewayServerBoot;

@SpringBootApplication
public class GameGatewayMain {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(GameGatewayMain.class, args);
		GatewayServerBoot serverBoot = context.getBean(GatewayServerBoot.class);
		serverBoot.startServer();
	}
}
