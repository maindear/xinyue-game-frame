package com.mygame.gateway.messages;

import com.xinyue.network.message.stream.AbstractGatewayGameMessage;
import com.xinyue.network.message.stream.EnumMesasageType;
import com.xinyue.network.message.stream.GameMessageMetadata;
import com.xinyue.network.message.stream.IGameMessage;
@GameMessageMetadata(messageId=2,messageType=EnumMesasageType.RESPONSE,serviceId=0)
public class HeartbeatMsgResponse extends AbstractGatewayGameMessage{
    @Override
    protected IGameMessage newCoupleImpl() {
        return new HeartbeatMsgRequest();
    }

    
}
