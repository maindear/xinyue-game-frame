package com.mygame.gateway.messages;

import com.google.protobuf.InvalidProtocolBufferException;
import com.mygame.gateway.messages.params.GatewayMessages.ConfirmRequestParam;
import com.xinyue.network.message.stream.AbstractGatewayGameMessage;
import com.xinyue.network.message.stream.EnumMesasageType;
import com.xinyue.network.message.stream.GameMessageMetadata;
import com.xinyue.network.message.stream.IGameMessage;

@GameMessageMetadata(messageId = 1, desc = "连接认证请求消息", messageType = EnumMesasageType.REQUEST, serviceId = 0)
public class ConfirmMesgRequest extends AbstractGatewayGameMessage {
	private ConfirmRequestParam param = null;

	@Override
	protected void parseFrom(byte[] bytes) throws InvalidProtocolBufferException {
		
		param = ConfirmRequestParam.parseFrom(bytes);

	}

	public ConfirmRequestParam getParam() {
		return param;
	}

	@Override
	protected IGameMessage newCoupleImpl() {
		return new ConfirmMsgResponse();
	}

}
