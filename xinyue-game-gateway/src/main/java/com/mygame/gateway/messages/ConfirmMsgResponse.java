package com.mygame.gateway.messages;

import com.mygame.gateway.messages.params.GatewayMessages.ConfirmResponseParam;
import com.xinyue.network.message.stream.AbstractGatewayGameMessage;
import com.xinyue.network.message.stream.EnumMesasageType;
import com.xinyue.network.message.stream.GameMessageMetadata;
import com.xinyue.network.message.stream.IGameMessage;

@GameMessageMetadata(messageId = 1, messageType = EnumMesasageType.RESPONSE, serviceId = 0)
public class ConfirmMsgResponse extends AbstractGatewayGameMessage {
    private ConfirmResponseParam.Builder builder = ConfirmResponseParam.newBuilder();

   

    @Override
    public ConfirmResponseParam.Builder getMessageBuilder() {
        return builder;
    }



    @Override
    protected IGameMessage newCoupleImpl() {
        return new ConfirmMesgRequest();
    }

}
