package com.mygame.gateway.common;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "xinyue.game.gateway.config")
public class GatewayServerConfig {
	/** 网关监听的端口 */
	private int serverPort;
	/** netty boss线程的个数，默认是 1 */
	private int bossThreadCount = 1;
	/** channel 工作线程的个数，默认是cpu的核数 */
	private int workThreadCount = 3;
	private int logicThreadCount = Runtime.getRuntime().availableProcessors();
	/** socket接收缓冲区大小 */
	private int recBufSize = 65535;
	/** 发送缓冲区大小 */
	private int sendBufSize = 65535;
	/** 达到压缩的消息最小大小，默认是4k */
	private int compressMessageThreshold = 1024 * 4;
	/** 等待连接认证的超时时间 */
	private int waiteConfirmTimeoutSecond = 10;
	/**
	 * 单个用户的限流请允许的每秒请求数量
	 */
	private double requestPerSecond = 10;
	/**
	 * 全局流量限制请允许每秒请求数量
	 */
	private double globalRequestPerSecond = 2000;

	/**
	 * 连接读写空闲时间
	 */
	private int serverChannelMaxIdleTimeSeconds = 120;
	/**
	 * 服务器ID，在整个集群之中，相同的serviceId下的serverId必须唯一。
	 */
	private int localServerId;
	/** 服务ID,整个集群之中，所有的serviceId必须是唯一的 */
	private int serviceId;

	public int getServerChannelMaxIdleTimeSeconds() {
		return serverChannelMaxIdleTimeSeconds;
	}

	public void setServerChannelMaxIdleTimeSeconds(int serverChannelMaxIdleTimeSeconds) {
		this.serverChannelMaxIdleTimeSeconds = serverChannelMaxIdleTimeSeconds;
	}

	public double getRequestPerSecond() {
		return requestPerSecond;
	}

	public void setRequestPerSecond(double requestPerSecond) {
		this.requestPerSecond = requestPerSecond;
	}

	public double getGlobalRequestPerSecond() {
		return globalRequestPerSecond;
	}

	public void setGlobalRequestPerSecond(double globalRequestPerSecond) {
		this.globalRequestPerSecond = globalRequestPerSecond;
	}

	public void setGlobalRequestPerSecond(int globalRequestPerSecond) {
		this.globalRequestPerSecond = globalRequestPerSecond;
	}

	public int getWaiteConfirmTimeoutSecond() {
		return waiteConfirmTimeoutSecond;
	}

	public void setWaiteConfirmTimeoutSecond(int waiteConfirmTimeoutSecond) {
		this.waiteConfirmTimeoutSecond = waiteConfirmTimeoutSecond;
	}

	public int getCompressMessageThreshold() {
		return compressMessageThreshold;
	}

	public void setCompressMessageThreshold(int compressMessageThreshold) {
		this.compressMessageThreshold = compressMessageThreshold;
	}

	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public int getBossThreadCount() {
		return bossThreadCount;
	}

	public void setBossThreadCount(int bossThreadCount) {
		this.bossThreadCount = bossThreadCount;
	}

	public int getWorkThreadCount() {
		return workThreadCount;
	}

	public void setWorkThreadCount(int workThreadCount) {
		this.workThreadCount = workThreadCount;
	}

	public int getRecBufSize() {
		return recBufSize;
	}

	public void setRecBufSize(int recBufSize) {
		this.recBufSize = recBufSize;
	}

	public int getSendBufSize() {
		return sendBufSize;
	}

	public void setSendBufSize(int sendBufSize) {
		this.sendBufSize = sendBufSize;
	}

	public int getLogicThreadCount() {
		return logicThreadCount;
	}

	public void setLogicThreadCount(int logicThreadCount) {
		this.logicThreadCount = logicThreadCount;
	}

	public int getLocalServerId() {
		return localServerId;
	}

	public void setLocalServerId(int localServerId) {
		this.localServerId = localServerId;
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

}
