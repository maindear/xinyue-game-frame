package com.mygame.common.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.beans.BeanUtils;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

public class GameBeanUtils {
    private static MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
    private static Map<String, MapperFacade> mapperFacadeCache = new ConcurrentHashMap<>();

    public static void shallowCopy(Object source, Object target) {
        BeanUtils.copyProperties(source, target);
    }

    public static <T> T deepCopy(Object source, Class<T> clazz) {
        MapperFacade mapper = mapperFacadeCache.get(clazz.getName());
        if (mapper == null) {
            mapperFactory.classMap(clazz, clazz).byDefault().register();
            mapper = mapperFactory.getMapperFacade();
        }
        T newObj = mapper.map(source, clazz);
        return newObj;
    }
}
