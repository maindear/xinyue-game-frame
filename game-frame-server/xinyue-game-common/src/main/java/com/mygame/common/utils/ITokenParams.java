package com.mygame.common.utils;

import java.time.Duration;

public interface ITokenParams {

	int expireDays();
}
