package com.mygame.common.utils;

import org.apache.commons.lang.time.DateFormatUtils;

public class GameTimeUtil {
    /**
     * 日期格式：yyyy-MM-dd HH:mm:ss
     */
    public final static String NORMAL_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static long SystemMillOffset = 0;
    
    public static String getStringDate(long millTime) {
        String value = DateFormatUtils.format(millTime, NORMAL_DATE_FORMAT);
        return value;
    }
    
    public static long currentTimeSecond() {
    	long time = SystemMillOffset + (System.currentTimeMillis() / 1000);
    	return time;
    }
}
