package com.mygame.common.model;

import java.time.Duration;

import com.mygame.common.utils.ITokenParams;

public class AccountToken implements ITokenParams {

	private String accountId;
	private String publicKey;
	private String gatewayIp;// 用于在网关验证是否是此网关的token，防止token串
	private long playerId;
	private int expireDays = 30;

	public void updateExpireDays(int days) {
		this.expireDays = days;
	}

	@Override
	public int expireDays() {
		return expireDays;
	}

	public long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getGatewayIp() {
		return gatewayIp;
	}

	public void setGatewayIp(String gatewayIp) {
		this.gatewayIp = gatewayIp;
	}

}
