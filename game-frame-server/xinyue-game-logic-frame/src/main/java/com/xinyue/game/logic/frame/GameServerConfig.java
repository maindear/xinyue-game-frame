package com.xinyue.game.logic.frame;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
@Configuration
@ConfigurationProperties(prefix="xinyue.game.server.config")
public class GameServerConfig {
	/** 业务处理请求消息的线程数 */
    private int logicThreads = 4;//业务处理线程数
    /** db处理线程数 ,默认是cpu的核数*/
    private int dbThreads = Runtime.getRuntime().availableProcessors();
    /** 处理从网关消息队列接收的消息的线程数 */
    private int gatewayConsumeThreads = 2;
	/** 游戏模块实例ID,针对同一个游戏模块，可以启动多个服务实例，实现负责均衡，比如登陆服务，排行榜服务，匹配服务 */
	private int gameModuleInstanceId;
	/** 游戏模块ID，具体指一个游戏的功功，比如排行榜，竞技场等 */
	private int gameModuleId;

    public int getGatewayConsumeThreads() {
        return gatewayConsumeThreads;
    }
    public void setGatewayConsumeThreads(int gatewayConsumeThreads) {
        this.gatewayConsumeThreads = gatewayConsumeThreads;
    }
    public int getDbThreads() {
        return dbThreads;
    }
    public void setDbThreads(int dbThreads) {
        this.dbThreads = dbThreads;
    }
    public int getLogicThreads() {
        return logicThreads;
    }
    public void setLogicThreads(int logicThreads) {
        this.logicThreads = logicThreads;
    }
	public int getGameModuleInstanceId() {
		return gameModuleInstanceId;
	}
	public void setGameModuleInstanceId(int gameModuleInstanceId) {
		this.gameModuleInstanceId = gameModuleInstanceId;
	}
	public int getGameModuleId() {
		return gameModuleId;
	}
	public void setGameModuleId(int gameModuleId) {
		this.gameModuleId = gameModuleId;
	}
    
    
}
