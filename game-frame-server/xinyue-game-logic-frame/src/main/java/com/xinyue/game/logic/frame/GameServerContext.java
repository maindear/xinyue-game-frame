package com.xinyue.game.logic.frame;

import java.util.Collection;
import com.xinyue.game.logic.frame.channel.GameChannel;
import com.xinyue.network.message.stream.IGameMessage;
import com.xinyue.network.message.stream.ServiceMessageHeader;

public class GameServerContext {

	private GameChannel gameChannel;
	private long playerId;
	private ServiceMessageHeader header;
	
	
	

	public GameServerContext(ServiceMessageHeader header,GameChannel gameChannel) {
		this.gameChannel = gameChannel;
		this.header = header;
	}
	
	public ServiceMessageHeader getHeader() {
		return header;
	}
	

	public long getPlayerId() {
		return this.header.getPlayerId();
	}
	public String getAccountId() {
		return this.header.getAttr().getAccountId();
	}



	public void sendToGateway(IGameMessage gameMessage) {
		gameChannel.sendToGateway(gameMessage);
	}
	public void broadcastToGateway(IGameMessage gameMessage,Collection<Long> playerIds) {
	    
	}
}
