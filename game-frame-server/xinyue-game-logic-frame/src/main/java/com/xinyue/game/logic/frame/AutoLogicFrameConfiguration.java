package com.xinyue.game.logic.frame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.xinyue.game.logic.frame.handler.GameMessageContext;
import com.xinyue.network.message.autoconfig.GameNetworkMessageAutoConfiguration;

@Configuration
public class AutoLogicFrameConfiguration {
    private Logger logger = LoggerFactory.getLogger(GameNetworkMessageAutoConfiguration.class);
	@Autowired
	private ApplicationContext appContext;
	
	@Bean
	public GatewayMessageReceiveService receiveService() {
		return new GatewayMessageReceiveService();
	}
	@Bean
	public GameRequestHandlerContext gameRequestHandlerContext() {
		return new GameRequestHandlerContext(appContext);
	}
	
	@Bean
	public GameServerConfig serverConfig() {
		return new GameServerConfig();
	}
    
    @Bean
    public GameMessageContext gameMessageContext() {
        logger.debug("开始初始化GameNetworkMessage");
        GameMessageContext gameMessageContext = new GameMessageContext();
        gameMessageContext.init(appContext);
        return gameMessageContext;
    }
	
}
