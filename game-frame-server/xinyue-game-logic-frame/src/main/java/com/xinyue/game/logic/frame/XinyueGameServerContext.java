package com.xinyue.game.logic.frame;

import org.springframework.context.ApplicationContext;

import com.mygame.common.concurrent.GameEventExecutorGroup;
import com.xinyue.game.logic.frame.channel.GameChannelGroup;

public class XinyueGameServerContext {

	private ApplicationContext applicatonContext;
	private GameEventExecutorGroup logicGroup;
	private GameEventExecutorGroup dbGroup;
	private GameChannelGroup gameChannelGroup;

	public XinyueGameServerContext(ApplicationContext applicationContext) {
		this(applicationContext, 0, 0);
	}

	public XinyueGameServerContext(ApplicationContext applicationContext, int logicThreads, int dbThreads) {
		if (logicThreads <= 0) {
			logicThreads = Runtime.getRuntime().availableProcessors();
		}
		if (dbThreads <= 0) {
			dbThreads = Runtime.getRuntime().availableProcessors() * 2;
		}
		logicGroup = new GameEventExecutorGroup(logicThreads,"GameLogicExecutor");
		dbGroup = new GameEventExecutorGroup(dbThreads,"DB-Executor");
		this.gameChannelGroup = new GameChannelGroup(logicGroup, applicationContext);
		this.applicatonContext = applicationContext;
	}

	public GameChannelGroup getGameChannelGroup() {
		return this.gameChannelGroup;
	}

	public GameEventExecutorGroup getLogicEventExecutorGroup() {
		return logicGroup;
	}

	public GameEventExecutorGroup getDBEventExecutorGroup() {
		return this.dbGroup;
	}

	public ApplicationContext getSpringContext() {
		return this.applicatonContext;
	}

}
