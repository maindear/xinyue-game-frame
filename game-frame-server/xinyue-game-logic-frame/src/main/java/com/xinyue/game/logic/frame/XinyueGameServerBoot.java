package com.xinyue.game.logic.frame;

import java.util.function.Consumer;
import org.apache.rocketmq.spring.support.DefaultRocketMQListenerContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.DefaultApplicationArguments;
import org.springframework.context.ApplicationContext;

public class XinyueGameServerBoot {

    public static XinyueGameServerContext serverContext;
    private static ApplicationContext springContext;
    private static boolean isStarted;
    private static ApplicationArguments arguments;
    private static Logger logger = LoggerFactory.getLogger(XinyueGameServerBoot.class);

    private XinyueGameServerBoot() {}

    public static void run(ApplicationContext springContext, String[] arguments) {
        XinyueGameServerBoot.arguments = new DefaultApplicationArguments(arguments);
        XinyueGameServerBoot.springContext = springContext;

        GameServerConfig serverConfig = springContext.getBean(GameServerConfig.class);
        modifyGatewayMessageConsumerThreadCount(serverConfig);
        int logicThreads = serverConfig.getLogicThreads();
        int dbThreads = serverConfig.getDbThreads();

        serverContext = new XinyueGameServerContext(springContext, logicThreads, dbThreads);
        isStarted = true;
        logger.info("服务启动成功");
    }

    private static void modifyGatewayMessageConsumerThreadCount(GameServerConfig serverConfig) {
        GatewayMessageReceiveService gatewayMessageReceiveService = springContext.getBean(GatewayMessageReceiveService.class);
        final String gatewayMessageTopic = gatewayMessageReceiveService.getTopic();
        foreachRocketMQListenerContainer(container -> {
            if (container.getTopic().equals(gatewayMessageTopic)) {
                if (serverConfig.getGatewayConsumeThreads() > 0) {
                    container.getConsumer().updateCorePoolSize(serverConfig.getGatewayConsumeThreads());
                    int coreSize = container.getConsumer().getDefaultMQPushConsumerImpl().getConsumeMessageService().getCorePoolSize();
                    logger.info("处理网关消息的消费者线程池被修改为：{} 个", coreSize);
                } else {
                    int coreSize = container.getConsumer().getDefaultMQPushConsumerImpl().getConsumeMessageService().getCorePoolSize();

                    logger.info("处理网关消息的消费者线程池核心数量：{} 个", coreSize);

                }
            }
        });

    }

    private static void foreachRocketMQListenerContainer(Consumer<DefaultRocketMQListenerContainer> consumer) {
        String[] names = springContext.getBeanNamesForType(DefaultRocketMQListenerContainer.class);
        if (names.length == 0) {
            logger.info("未找到RocketMQ的消费者监听容器");
            return;
        }
        DefaultRocketMQListenerContainer rocketMQListenerContainer = null;
        for (String name : names) {
            rocketMQListenerContainer = (DefaultRocketMQListenerContainer) springContext.getBean(name);
            consumer.accept(rocketMQListenerContainer);

        }
    }

    public static ApplicationContext getSpringContext() {
        return springContext;
    }

    public static boolean isStarted() {
        return isStarted;
    }

    public static ApplicationArguments getApplcationArguments() {
        return arguments;
    }

}
