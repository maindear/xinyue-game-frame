package com.xinyue.framework.dao;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;

@Configuration
@ComponentScan({"com.xinyue.framework.dao"})

public class GameDaoAutoConfiguration {
    private Logger logger = LoggerFactory.getLogger(GameDaoAutoConfiguration.class);
	@Autowired
	private NacosDiscoveryProperties nacosDiscoveryProperties;
	
	@PostConstruct
	public void init() {
		logger.info("Dao Redis存储的命令空间为：{}",nacosDiscoveryProperties.getNamespace());
	}
    @Bean
    @ConditionalOnMissingBean
    public IGameDaoExecutorFactory gameDaoExecutorFactory() {
        return new DefaultGameDaoEventExecutorFactory();
    }
}
