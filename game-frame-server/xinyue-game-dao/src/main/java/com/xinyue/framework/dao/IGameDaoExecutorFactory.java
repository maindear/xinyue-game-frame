package com.xinyue.framework.dao;

import com.mygame.common.concurrent.GameEventExecutorGroup;

public interface IGameDaoExecutorFactory {

    GameEventExecutorGroup getEventExecutorGroup();
}
