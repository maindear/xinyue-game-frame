package com.xinyue.rocketmq;

import java.util.List;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.remoting.common.RemotingHelper;

public class ProducerTest {

    public static void main(String[] args) throws Exception {
        DefaultMQProducer producer = new DefaultMQProducer("example_group_name");
        producer.setNamesrvAddr("172.16.139.80:9876");
        producer.start();
        int a = 0;
        for (int i = 0; i < 150000; i++) {
            Thread.sleep(500);
            a++;
            if(a > 9) {
                a = 1;
            }
            //Create a message instance, specifying topic, tag and message body.
            Message msg = new Message("TopicTest","*",
                    ("Hello RocketMQ " + a).getBytes(RemotingHelper.DEFAULT_CHARSET));
            SendResult sendResult = producer.send(msg, new MessageQueueSelector() {
            @Override
            public MessageQueue select(List<MessageQueue> mqs, Message msg, Object arg) {
                Integer id = (Integer) arg;
                int index = id % mqs.size();
                System.out.println("msqs:" + mqs.size() + ",id:" + id + ",index:"+index);
                return mqs.get(index);
            }
            }, a);

            System.out.printf("%s%n", sendResult);
        }
        //server shutdown
        producer.shutdown();
    }
}
