package com.xinyue.mqsystem.event;

import java.lang.reflect.Method;

public class GameEventDispatcherInfo {

    private Object target;
    private Method method;
    public GameEventDispatcherInfo(Object target, Method method) {
        super();
        this.target = target;
        this.method = method;
    }
    public Object getTarget() {
        return target;
    }
    public Method getMethod() {
        return method;
    }
    
    
}
