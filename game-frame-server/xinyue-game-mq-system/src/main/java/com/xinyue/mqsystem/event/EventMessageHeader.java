package com.xinyue.mqsystem.event;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.util.CharsetUtil;

public class EventMessageHeader {

    private int eventId;
    private String id;
    private int fromServerId;

    public static int readEventId(ByteBuf byteBuf) {
        byteBuf.markReaderIndex();
        int eventId = byteBuf.readInt();
        byteBuf.resetReaderIndex();
        return eventId;
    }

    public void read(ByteBuf byteBuf) {
        this.id = byteBuf.readCharSequence(32, CharsetUtil.UTF_8).toString();
        this.fromServerId = byteBuf.readInt();
    }

    public ByteBuf write() {
        ByteBuf byteBuf = Unpooled.buffer(36);
        byteBuf.writeInt(eventId);
        byteBuf.writeCharSequence(this.id, CharsetUtil.UTF_8);
        byteBuf.writeInt(fromServerId);
        return byteBuf;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    

    public int getFromServerId() {
		return fromServerId;
	}

	public void setFromServerId(int fromServerId) {
		this.fromServerId = fromServerId;
	}

	@Override
    public String toString() {
        return "EventMessageHeader [eventId=" + eventId + ", id=" + id + ", fromServerId=" + fromServerId + "]";
    }



}
