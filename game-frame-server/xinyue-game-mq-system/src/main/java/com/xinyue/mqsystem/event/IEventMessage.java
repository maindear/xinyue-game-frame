package com.xinyue.mqsystem.event;

import io.netty.buffer.ByteBuf;

public interface IEventMessage {

   
   EventMessageHeader getHeader();
   void read(ByteBuf byteBuf) throws Exception;
   byte[] write();
}
