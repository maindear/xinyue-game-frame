package com.xinyue.mqsystem;

import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.xinyue.mqsystem.mq.GameMQTemplate;

@Configuration
public class AutoGameMQSystemConfiguration {

	@Autowired
	private RocketMQTemplate rocketMQTemplate;

	@Bean
	public GameMQTemplate gameMQTemplate() {
		return new GameMQTemplate(rocketMQTemplate);
	}
}
