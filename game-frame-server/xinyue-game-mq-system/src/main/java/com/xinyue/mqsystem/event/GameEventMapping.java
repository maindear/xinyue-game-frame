package com.xinyue.mqsystem.event;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface GameEventMapping {

    Class<? extends IEventMessage> value();
    /** TODO 用于对同一个事件处理时的排序,值越低，优先级越高 */
    int Order() default 0;
}
