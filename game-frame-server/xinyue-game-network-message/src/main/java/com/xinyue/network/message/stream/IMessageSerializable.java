package com.xinyue.network.message.stream;

import com.google.protobuf.InvalidProtocolBufferException;

import io.netty.buffer.ByteBuf;

public interface IMessageSerializable {

    void read(ByteBuf byteBuf) throws InvalidProtocolBufferException;
    ByteBuf write();
}
