package com.xinyue.network.message.web;

import com.xinyue.network.message.error.GameErrorException;
import com.xinyue.network.message.error.IServerError;

public abstract class AbstractHttpRequestParam {
    protected IServerError error;

    public void checkParam() {
        haveError();
        if (error != null) {
            throw new GameErrorException.Builder(error).message("异常类:{}", this.getClass().getName()).build();
        }
    }
    protected abstract void haveError();
}
