package com.xinyue.network.message.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.xinyue.network.message.error.GameErrorException;

/**
 * 
 * @ClassName: GlobalExceptionCatch
 * @Description: 全局异常捕获
 * @author: wgs
 * @date: 2019年3月15日 下午10:27:55
 */
@ControllerAdvice
public class GlobalExceptionCatch {
    private Logger logger = LoggerFactory.getLogger(GlobalExceptionCatch.class);

    @ResponseBody
    @ExceptionHandler(value = Throwable.class)
    public ResponseEntity<JSONObject> exceptionHandler(Throwable ex) {
        ResponseEntity<JSONObject> response = null;
        
        if (ex instanceof GameErrorException) {
            GameErrorException gameError = (GameErrorException) ex;
            response = new ResponseEntity<>(gameError.getError());
            logger.error("服务器异常,{}", ex.getMessage());
        } else {
            response = new ResponseEntity<>(-1, "未知异常:" + ex.getMessage());
            logger.error("服务器未知异常,{}",ex.getClass().getName(), ex);
        }
        return response;
    }

}
