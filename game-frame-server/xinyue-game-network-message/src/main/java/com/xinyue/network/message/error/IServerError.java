package com.xinyue.network.message.error;

public interface IServerError {

    int getErrorCode();
    String getErrorDesc();
    
}
