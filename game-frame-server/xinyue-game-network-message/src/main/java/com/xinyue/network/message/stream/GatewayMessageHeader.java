package com.xinyue.network.message.stream;

import io.netty.buffer.ByteBuf;

public class GatewayMessageHeader extends AbstractMessageHeader{

    //在网关处理消息的解压与解密操作
    //是否压缩
    private boolean compress;
    //是否加密
    private boolean encrypt;
    private int errorCode;
    private int gameModuleId;
    private EnumMesasageType messageType;
    
    public GatewayMessageHeader() {
        super();
        int size = 7;//本对象写入的字节数量
        this.addHeaderLength(size);
    }
    
    
    
    public EnumMesasageType getMessageType() {
		return messageType;
	}



	public void setMessageType(EnumMesasageType messageType) {
		this.messageType = messageType;
	}



	public int getGameModuleId() {
		return gameModuleId;
	}



	public void setGameModuleId(int serviceId) {
		this.gameModuleId = serviceId;
	}



	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}


	@Override
    public void read(ByteBuf byteBuf) {
        super.read(byteBuf);//选择读取父类的数据
        this.gameModuleId = byteBuf.readShort();
        //1 是加密，0 是不加密
        this.encrypt = byteBuf.readByte() == 1 ? true : false;
        this.compress = byteBuf.readByte() == 1 ? true : false;
    }
    
    @Override
    public ByteBuf write() {
        //先写入父类信息
        ByteBuf byteBuf = super.write();
        byteBuf.writeInt(this.errorCode);
        byteBuf.writeByte(this.getMessageType().getType());
        byteBuf.writeByte(this.encrypt ? 1 : 0);
        byteBuf.writeByte(this.compress ? 1 : 0);
        return byteBuf;
    }
    public boolean isCompress() {
        return compress;
    }
    public void setCompress(boolean compress) {
        this.compress = compress;
    }
    public boolean isEncrypt() {
        return encrypt;
    }
    public void setEncrypt(boolean encrypt) {
        this.encrypt = encrypt;
    }



	@Override
	public String toString() {
		return "GatewayMessageHeader [getMessageType()=" + getMessageType() + ", getServiceId()=" + getGameModuleId()
				+ ", getErrorCode()=" + getErrorCode() + ", getMessageId()=" + getMessageId() + ", getMessageSeqId()="
				+ getMessageSeqId() + ", getVersion()=" + getVersion() + "]";
	}
    
    
    
    
    
}
