package com.xinyue.network.message.stream;

import com.google.protobuf.InvalidProtocolBufferException;

import io.netty.buffer.ByteBuf;

/**
 * 
 * @ClassName: GatewayGameMessage 
 * @Description: 客户端与网关交互的消息
 * @author: wang guang shuai
 * @date: 2020年3月10日 下午5:07:15
 */
public abstract class AbstractGatewayGameMessage extends AbstractGameMessage{
    private GatewayMessageHeader header;
    

    @Override
    protected IMessageHeader createHeader() {
        header = new GatewayMessageHeader();
        return header;
    }
    @Override
    public void read(ByteBuf byteBuf) {
        throw new UnsupportedOperationException("网关消息不能调用这个方法，需要使用：readTransferMessage");
    }
    
    public void readTransferMessage(TransferMessage transferMessage) throws InvalidProtocolBufferException {
        IMessageHeader newHeader = transferMessage.getHeader();
        this.header.setMessageSeqId(newHeader.getMessageSeqId());
        this.header.setMessageId(newHeader.getMessageId());
        this.header.setMessageType(newHeader.getMessageType());
        this.header.setGameModuleId(newHeader.getGameModuleId());
        ByteBuf byteBuf = transferMessage.getBody();
        if (byteBuf != null && byteBuf.isReadable()) {
            byte[] bytes = byteBuf.array();
            this.parseFrom(bytes);
        }
    }
    
    
    public <T extends IGameMessage> T newCouple() {
        @SuppressWarnings("unchecked")
		T t = (T) newCoupleImpl();
        GatewayMessageHeader coupleHeader = (GatewayMessageHeader) t.getHeader();
        coupleHeader.setMessageSeqId(header.getMessageSeqId());
        return t;
    }
    
    protected abstract IGameMessage newCoupleImpl();
    
    
}
