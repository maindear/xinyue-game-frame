package com.xinyue.network.message.stream;

import com.alibaba.fastjson.JSON;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.CompositeByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.util.CharsetUtil;

public class ServiceMessageHeader extends AbstractMessageHeader {
	
    private int fromGameModuleInstanceId;
    private int toGameModuleInstanceId;
    private long playerId;
    private int errorCode;
    private int gameModuleId;
	// 消息类型
	private EnumMesasageType messageType;
    // 留下一个扩展的对象，添加新的字段信息时，就不需要修改协议序列化了。
    private HeaderAttribute attr = new HeaderAttribute();
    public ServiceMessageHeader() {
        super();
        int size = 22;// short int,int  long的长度,还有4字节是attr数据的长度
        this.addHeaderLength(size);
    }

    
    public EnumMesasageType getMessageType() {
		return messageType;
	}


	public void setMessageType(EnumMesasageType messageType) {
		this.messageType = messageType;
	}


	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}


	public int getGameModuleId() {
		return gameModuleId;
	}


	public void setGameModuleId(int serviceId) {
		this.gameModuleId = serviceId;
	}


	@Override
    public void read(ByteBuf byteBuf) {
        super.read(byteBuf);
        this.errorCode = byteBuf.readInt();
        this.gameModuleId = byteBuf.readShort();
        this.fromGameModuleInstanceId = byteBuf.readInt();
        this.toGameModuleInstanceId = byteBuf.readInt();
        this.messageType = EnumMesasageType.getType(byteBuf.readByte());
        this.playerId = byteBuf.readLong();
        int attrLength = byteBuf.readInt();
        // 读取attr对象的json串信息
        String json = byteBuf.readCharSequence(attrLength, CharsetUtil.UTF_8).toString();
        this.attr = JSON.parseObject(json, HeaderAttribute.class);
    }

    private ByteBuf serializedAttr() {
        if (attr == null) {
            attr = new HeaderAttribute();
        }
        String json = JSON.toJSONString(attr);
        byte[] bytes = json.getBytes(CharsetUtil.UTF_8);
        return Unpooled.wrappedBuffer(bytes);
    }

    @Override
    public ByteBuf write() {
        ByteBuf byteBuf =  super.write();
        byteBuf.writeInt(this.errorCode);
        byteBuf.writeShort(this.gameModuleId);
        byteBuf.writeInt(this.fromGameModuleInstanceId);
        byteBuf.writeInt(this.toGameModuleInstanceId);
        byteBuf.writeByte(this.messageType.getType());
        byteBuf.writeLong(playerId);
        ByteBuf attrBuf = this.serializedAttr();
        byteBuf.writeInt(attrBuf.readableBytes());
        CompositeByteBuf result = Unpooled.compositeBuffer(2);
        result.addComponent(true,byteBuf);
        result.addComponent(true,attrBuf);
        return result;
    }

    public HeaderAttribute getAttr() {
        return attr;
    }

    public void setAttr(HeaderAttribute attr) {
        this.attr = attr;
    }

    public int getFromGameModuleInstanceId() {
        return fromGameModuleInstanceId;
    }

    public void setFromGameModuleInstanceId(int fromServerId) {
        this.fromGameModuleInstanceId = fromServerId;
    }

    public int getToGameModuleInstanceId() {
        return toGameModuleInstanceId;
    }

    public void setToGameModuleInstanceId(int toServerId) {
        this.toGameModuleInstanceId = toServerId;
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }


	@Override
	public String toString() {
		return "ServiceMessageHeader [getErrorCode()=" + getErrorCode() + ", getServiceId()=" + getGameModuleId()
				+ ", getFromServerId()=" + getFromGameModuleInstanceId() + ", getToServerId()=" + getToGameModuleInstanceId()
				+ ", getPlayerId()=" + getPlayerId() + ", getMessageId()=" + getMessageId() + ", getMessageSeqId()="
				+ getMessageSeqId() + ", getVersion()=" + getVersion() + "]";
	}
    
    
}
