package com.xinyue.network.message.stream;

import io.netty.buffer.ByteBuf;
/**
 * 
 * @ClassName: TransferMessage 
 * @Description: 传输的信息内容
 * @author: wang guang shuai
 * @date: 2020年3月10日 下午12:26:43
 */
public class TransferMessage {
    
    private IMessageHeader header;
    private ByteBuf body;
    
    
    public TransferMessage(IMessageHeader header, ByteBuf body) {
        super();
        this.header = header;
        this.body = body;
    }
    public IMessageHeader getHeader() {
        return header;
    }
    public ByteBuf getBody() {
        return body;
    }
    
    
}
