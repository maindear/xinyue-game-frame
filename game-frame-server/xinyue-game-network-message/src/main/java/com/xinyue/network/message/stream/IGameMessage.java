package com.xinyue.network.message.stream;

public interface IGameMessage extends IMessageSerializable{
    
   IMessageHeader getHeader();
}
