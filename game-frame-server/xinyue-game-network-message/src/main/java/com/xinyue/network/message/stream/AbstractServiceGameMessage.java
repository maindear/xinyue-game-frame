package com.xinyue.network.message.stream;

/**
 * 
 * @ClassName: ServiceGameMessage
 * @Description: 网关与内部服务之间交互的消息，一般用于一请一答的交互方式，即有请求必有回应的方式。
 * @author: wang guang shuai
 * @date: 2020年3月10日 下午5:08:21
 */
public abstract class AbstractServiceGameMessage extends AbstractGameMessage {
	private ServiceMessageHeader header;

	@Override
	protected IMessageHeader createHeader() {
		header = new ServiceMessageHeader();
		return header;
	}

	public <T extends IGameMessage> T newCouple() {
		@SuppressWarnings("unchecked")
		T t = (T) this.newCoupleImpl();
		ServiceMessageHeader coupleHeader = (ServiceMessageHeader) t.getHeader();
		coupleHeader.setFromGameModuleInstanceId(header.getToGameModuleInstanceId());
		coupleHeader.setToGameModuleInstanceId(header.getFromGameModuleInstanceId());
		coupleHeader.setMessageSeqId(header.getMessageSeqId());
		coupleHeader.setPlayerId(header.getPlayerId());
		return t;
	}

	protected abstract IGameMessage newCoupleImpl();
	
	
}
