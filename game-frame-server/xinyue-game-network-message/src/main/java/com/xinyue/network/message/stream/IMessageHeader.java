package com.xinyue.network.message.stream;

public interface IMessageHeader extends IMessageSerializable{

    int getHeaderByteSize();

	// 请求消息ID
    int getMessageId();

	// 游戏模块ID
    int getGameModuleId();

	// 请求消息序列ID
    int getMessageSeqId();

	// 错误码
    int getErrorCode();

	// 消息类型
    EnumMesasageType getMessageType();

	// 设置消息ID
    void setMessageId(int messageId);

	// 设置游戏模块ID
	void setGameModuleId(int gameModeulId);

	// 设置错误码
    void setErrorCode(int errorCode);

	// 设置消息类型
    void setMessageType(EnumMesasageType mesasageType);
    
}
