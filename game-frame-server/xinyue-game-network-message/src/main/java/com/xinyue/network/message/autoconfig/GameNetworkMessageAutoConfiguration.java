package com.xinyue.network.message.autoconfig;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.xinyue.network.message.web")
public class GameNetworkMessageAutoConfiguration {

   

}
