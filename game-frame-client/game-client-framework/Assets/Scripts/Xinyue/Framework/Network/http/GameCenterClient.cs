﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
/// <summary>
/// 用户中心请求客户端，主要用于和用户中心服务通信
/// </summary>
/// 

public delegate void GameCenterResponse(JToken data);
public class GameCenterClient : BaseJsonHttpClient<GameCenterClient>
{
    private const string ServiceName = "/game-center-server/";
    public const string UserLogin = ServiceName + "user/login";
    public const string CreatePlayer = ServiceName + "user/createPlayer";
    public const string SelectGateway = ServiceName + "user/selectGameGateway";
    public string UserToken { get; set; }


    private GameCenterClient() { }
    protected override string getTargetUrl()
    {
        return GameClientConfig.GameCenterUrl;
    }

    public new IEnumerator SendPostRequest(string path, object param, GameCenterResponse response, OnHttpError onHttpError)
    {
        Dictionary<string, string> header = null;
        if (UserToken != null)
        {
            header = new Dictionary<string, string>();
            header.Add("user-token", UserToken);
        }
        return SendPostRequest(path, param, header, response, onHttpError);
    }


}
