﻿using System;
public class HeartbeatMessageResponse : AbstractGameMessage
{
    public HeartbeatMessageResponse()
    {
    }

    public override int GetMessageID()
    {
        return 2;
    }

    public override EnumMessageType GetMessageType()
    {
        return EnumMessageType.RESPONSE;
    }

    public override int GetServiceID()
    {
        return 0;
    }

    public override string Log()
    {
        string msg = "心跳返回消息";
        return msg;
    }

    public override void Read(byte[] body)
    {

    }

    public override byte[] Write()
    {
        return null;
    }
}
