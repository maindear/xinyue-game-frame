﻿using System;
public class GatewayInfo
{
    public int Id { get; set; }
    public string Ip { get; set; }
    public int Port { get; set; }
    public string Token { get; set; }
    public string RsaPrivateKey { get; set; }
    public string DesSecreteKey { get; set; }

    public override string ToString()
    {
        return Id + "," + Ip + "," + Port;
    }

}
