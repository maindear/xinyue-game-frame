﻿using System;
using com.xinyue.gateway.messages;

public class ConfirmMessageResponse : AbstractGameMessage
{
    public ConfirmResponseParam param;

 
    
    public override int GetMessageID()
    {
        return 1;
    }

    public override EnumMessageType GetMessageType()
    {
        return EnumMessageType.RESPONSE;
    }

    public override int GetServiceID()
    {
        return 1;
    }

    public override string Log()
    {
        return "连接认证响应消息";
    }

    public override void Read(byte[] body)
    {
        param = ConfirmResponseParam.Parser.ParseFrom(body);
    }
}
