﻿using System;
using com.xinyue.gateway.messages;
using Google.Protobuf;

public class ConfirmMessageRequest : AbstractGameMessage
{
    public readonly ConfirmRequestParam param = new ConfirmRequestParam();
    public override int GetMessageID()
    {
        return 1;
    }

    public override EnumMessageType GetMessageType()
    {
        return EnumMessageType.REQUEST;
    }

    public override int GetServiceID()
    {
        return 1;
    }

    public override string Log()
    {
        Type type = this.GetType();
        string log = type.FullName + ", 连接认证消息";
        return log;
    }

    public override void Read(byte[] body)
    {
    }

    public override byte[] Write()
    {
        return param.ToByteArray();
    }

    public class RequestBody
    {
        public string Token { get; set; }
    }
}
