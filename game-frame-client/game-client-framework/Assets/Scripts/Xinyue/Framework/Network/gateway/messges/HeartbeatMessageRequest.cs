﻿using System;
public class HeartbeatMessageRequest : AbstractGameMessage
{
    public HeartbeatMessageRequest()
    {
    }

    public override int GetMessageID()
    {
        return 2;
    }

    public override EnumMessageType GetMessageType()
    {
        return EnumMessageType.REQUEST;
    }

    public override int GetServiceID()
    {
        return 0;
    }

    public override string Log()
    {
        string msg = "心跳消息";
        return msg;
    }

    public override void Read(byte[] body)
    {

    }

    public override byte[] Write()
    {
        return null;
    }
}
