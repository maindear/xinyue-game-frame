﻿using System;
public enum EnumMessageType
{
    REQUEST = 1,
    RESPONSE = 2,
    PUSH = 3
}
