﻿using System;
using System.Threading;
using Assets.Scripts.network.socket;
using UnityEngine;

namespace Assets.Scripts.network.socket
{
    public class GameSocketManager
    {
        private ISocketManager socketManager;

        private SocketSendManager sendManager;
        private SocketConnectManager connectManager;
        private SocketReceiveManager receiveManager;


        private ServerMessageDispatch serverMessageDispatch;
        public GameSocketManager(string host, int port)
        {
          
            serverMessageDispatch = new ServerMessageDispatch();
            socketManager = new SocketManager(host, port);
            sendManager = new SocketSendManager(serverMessageDispatch, socketManager);
            connectManager = new SocketConnectManager(socketManager);
            receiveManager = new SocketReceiveManager(socketManager, serverMessageDispatch);
        }

        public void AddSocketStateChangeListener(Action<SocketStateEnum> listener, Type classType)
        {
            Debug.Log("业务：" + classType.FullName + "类添加网络状态监听");
            connectManager.SocketStateChangeEvent += listener;
        }

        public void RemoveSocketStateChangeListener(Action<SocketStateEnum> socketState, Type sceneClassType)
        {
            Debug.Log("业务：" + sceneClassType.FullName + "添加网络状态监听");
            connectManager.SocketStateChangeEvent -= socketState;
        }

        public void UpdateSocketState(SocketStateEnum socketState)
        {
            connectManager.UpdateSocketState(socketState);
        }


        public void Shutdown()
        {
            socketManager.CloseConnect();
            sendManager.Shutdown();
            receiveManager.Shutdown();
            connectManager.Shutdown();
        }

        public void AddPushMessageCallback(int messageId, PushMessageInfo pushMessageInfo)
        {
            serverMessageDispatch.AddPushMessageCallback(messageId, pushMessageInfo);
        }

        public void ConnectServer(Action<SocketStateEnum> callback)
        {

            connectManager.ConnectServer((socketSate) =>
            {
                if (socketSate == SocketStateEnum.ConnectSuccess)
                {
                    receiveManager.StartReceive();
                }
                callback.Invoke(socketSate);
            });
        }

        public void CloseConnect()
        {
            connectManager.CloseSocket();
        }

        public bool IsConnected()
        {
            return socketManager.IsConnected();
        }
        public bool Send<T>(IGameMessage message, Action<IGameMessage> callback) where T : IGameMessage
        {
            return sendManager.Send<T>(message, callback);
        }


    }
}
