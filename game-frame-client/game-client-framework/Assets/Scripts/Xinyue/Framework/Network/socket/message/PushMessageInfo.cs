﻿using System;
public class PushMessageInfo
{
    public Type GameMessageType { get; set; }
    public Action<IGameMessage> Callback { get; set; }
}
