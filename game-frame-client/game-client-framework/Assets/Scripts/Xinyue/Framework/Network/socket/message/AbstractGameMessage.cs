﻿using System;
using System.Collections.Generic;

public abstract class AbstractGameMessage : IGameMessage
{
    private GameMessageHeader header;

    public AbstractGameMessage()
    {
        this.InitHeader();
    }
    private void InitHeader()
    {
        header = new GameMessageHeader
        {
            MessageId = this.GetMessageID(),
            ServiceId = this.GetServiceID(),
            MessageType = this.GetMessageType()
        };
    }
    public GameMessageHeader GetHeader()
    {
        return header;
    }

    public virtual void Read(byte[] body) { }

    public virtual  byte[] Write()
    {
        return null;
    }

    public abstract int GetMessageID();
    public abstract int GetServiceID();
    public abstract EnumMessageType GetMessageType();
    public abstract string Log();


}
