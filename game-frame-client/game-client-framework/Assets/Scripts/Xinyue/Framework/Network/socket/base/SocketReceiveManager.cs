﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.network.socket
{
    class SocketReceiveManager
    {
        private ISocketManager socketManager;

        private ServerMessageDispatch serverMessageDispatch;
        private volatile bool shuwdown;
        //接收数据线程
        private EventExecutor receiveExecutor = new EventExecutor();
        public SocketReceiveManager(ISocketManager socketManager,ServerMessageDispatch serverMessageDispatch)
        {
            this.socketManager = socketManager;
          
            this.serverMessageDispatch = serverMessageDispatch;
            
        }
        public void Shutdown()
        {
            shuwdown = true;
            receiveExecutor.Shutdown();
        }

        public void StartReceive()
        {
            receiveExecutor.Execute(() =>
            {
                ReadData((byteBuf) =>
                {
                    serverMessageDispatch.DispatchMessage(byteBuf);
                });
            });
        }
        private void ReadData(Action<ByteBuf> callback)
        {
            byte[] buffer = new byte[1024 * 16];
            ByteBuf byteBuf = new ByteBuf(buffer);
            Debug.Log("启动服务端消息读取");
            while (!shuwdown)
            {
                if (!socketManager.IsConnected())
                {
                    Debug.Log("读取消息时，连接已断开");
                    break;
                }
                try
                {
                    //循环使用buffer,不用每次都创建一次buffer
                    int offset = byteBuf.WriteIndex;
                    int readSize = socketManager.Receive(buffer, offset, buffer.Length - offset);
                    int totalOffset = 0;
                    if (readSize > 0)
                    {
                        Debug.Log("接收消息大小：" + readSize);
                        totalOffset = offset + readSize;
                    }

                    if (totalOffset >= buffer.Length - 128)
                    {
                        byteBuf.DiscardReadBytes();
                        offset = byteBuf.WriteIndex;
                    }
                    byteBuf.WriteIndex = byteBuf.WriteIndex + readSize;

                    int size = byteBuf.ReadableBytes();
                    if (size > 4)
                    {
                        byteBuf.MarkReaderIndex();

                        size = byteBuf.ReadInt() - 4;
                        //粘包处理
                        if (size <= byteBuf.ReadableBytes() && byteBuf.ReadableBytes() > 0)
                        {
                            //可以取出一个完整的包
                            byteBuf.ResetReaderIndex();
                            callback?.Invoke(byteBuf);

                        }
                        else //不足一个完整包的数据
                        {
                            //断包处理,等待数据的到来
                            byteBuf.ResetReaderIndex();
                            Debug.Log("剩余可读取字节数：" + byteBuf.ReadableBytes());
                        }
                    }

                }
                catch (Exception e)
                {
                    Debug.LogError("数据解析错误：" + e);
                    byteBuf.Clear();
                }
            }
            Debug.Log("结束服务器消息读取");
        }

    }
}
