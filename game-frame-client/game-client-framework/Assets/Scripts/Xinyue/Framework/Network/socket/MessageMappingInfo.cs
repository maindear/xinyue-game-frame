﻿using System;
public class MessageMappingInfo
{

    public int ClientSeqID { get; set; }
    public IGameMessage ResponseMessage { get; set; }
    public Action<IGameMessage> Action { get; set; }
}
