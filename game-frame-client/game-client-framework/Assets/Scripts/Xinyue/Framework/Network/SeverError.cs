﻿using System;
public class ServerError
{
    public const int HttpError = -1000;//Http网络错误
    public const int UnknowException = -1001;
    public const int NoGateway = 9;//没有可用的网关
    public int code;
    public string errorMsg;

    public ServerError(int code, string errorMsg)
    {
        this.code = code;
        this.errorMsg = errorMsg;
    }

    public override string ToString()
    {
        string message = "错误码:" + this.code + "," + errorMsg;
        return message;
    }


}
