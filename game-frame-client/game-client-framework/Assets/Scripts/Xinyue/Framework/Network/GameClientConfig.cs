﻿using System.Collections;

public class GameClientConfig
{

    #if  UNITY_EDITOR
    public static string GameCenterUrl = "http://localhost:5001";
    //public static string GameCenterUrl = "http://xinyue.yicp.vip";
    #elif UNITY_ANDROID
    public static string GameCenterUrl = "http://xinyue.yicp.vip";
    #endif
    /// <summary>
    /// Http请求的超时时间，单位是秒
    /// </summary>
    public static  int HttpRequestTimeout = 32;
    /// <summary>
    /// 消息压缩的分界值，超过此值，将压缩消息发送。
    /// </summary>
    public static int CompressSize = 1024;
    /// <summary>
    /// 网关连接的超时时间，单位是毫秒
    /// </summary>
    public static  int SocketConnectTimeout = 15000;//mill
    /// <summary>
    /// 是否开启心跳,false不开启
    /// </summary>
    public static  bool EnableHeartbeat = true;

}
