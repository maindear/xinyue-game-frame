﻿using Newtonsoft.Json;


public class JsonUtil
{
    public static string JsonToString(object value)
    {
         return JsonConvert.SerializeObject(value);
    }

    public static T JsonToObject<T>(string json)
    {
         return JsonConvert.DeserializeObject<T>(json);
    }
}
