﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace xinyue.game.utils
{
    class ScreenUtil
    {

        /// <summary>
        /// 判断物品位置是否在屏幕内
        /// </summary>
        /// <param name="gameObjecPosition"></param>
        /// <returns></returns>
        public static  bool IsInScreen(Vector3 gameObjecPosition,Camera camera)
        {
            Vector3 screenPos = camera.WorldToScreenPoint(gameObjecPosition);
            return IsInScreenWeight(gameObjecPosition,camera) && IsInScreenHeight(gameObjecPosition,camera);
        }
        public static bool IsInScreenWeight(Vector3 gameObjectPosition,Camera camera)
        {
            Rect rect = camera.rect;
            Vector3 screenPos = camera.WorldToScreenPoint(gameObjectPosition);
          
            bool flag = screenPos.x > 0 && screenPos.x  < rect.width;
            return flag;
        }
        public static bool IsInScreenHeight(Vector3 gameObjectPosition,Camera camera)
        {
            Rect rect = camera.rect;
            Vector3 screenPos = camera.WorldToScreenPoint(gameObjectPosition);
            bool flag = screenPos.y > 0 && screenPos.y < rect.height;
            return flag;
        }

    }
}
