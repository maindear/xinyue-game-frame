﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class StringUtil
{

    public static bool IsNotBlank(string value)
    {
        return value != null && value.Length > 0;
    }
    public static bool IsBlank(string value)
    {
        if (value == null)
        {
            return true;
        }
        if (value.Equals(""))
        {
            return true;
        }
        return false;
    }
    public static string bytesToString(byte[] bytes)
    {
        return System.Text.Encoding.Default.GetString(bytes);
    }
    public static byte[] stringToBytes(string value)
    {
        return System.Text.Encoding.Default.GetBytes(value);
    }
}
