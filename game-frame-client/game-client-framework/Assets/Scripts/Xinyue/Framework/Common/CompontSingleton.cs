﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompontSingleton<T> : MonoBehaviour where T: MonoBehaviour
{

    private static T _instance;

    public static T Instance {
        get
        {
            if(_instance == null)
            {
                _instance = (T)FindObjectOfType(typeof(T));
            }
            return _instance;

        }

    }
}
